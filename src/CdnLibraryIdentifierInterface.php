<?php

namespace Drupal\cdn_library;

use Drupal\cdn_library\Annotation\CdnLibraryUrl;

interface CdnLibraryIdentifierInterface {

  /**
   * @return \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface
   */
  public function getProvider();

  /**
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl|NULL $url
   *
   * @return string
   */
  public function normalizedName(CdnLibraryUrl $url = NULL);

  /**
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl|NULL $url
   *
   * @return string
   */
  public function normalizedVersion(CdnLibraryUrl $url = NULL);

}
