<?php

namespace Drupal\cdn_library;

use Drupal\cdn_library\Annotation\CdnLibraryUrl;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Psr\Http\Message\ResponseInterface;

class CdnLibraryResponse extends CacheableJsonResponse {

  use DependencySerializationTrait;

  /**
   * @var array
   */
  protected $json;

  /**
   * @var \Drupal\cdn_library\Annotation\CdnLibraryUrl
   */
  protected $url;

  /**
   * {@inheritdoc}
   */
  public function __construct(CdnLibraryUrl $url, ResponseInterface $response = NULL) {
    $data = $response ? $response->getBody()->getContents() : '[]';
    $status_code = $response ? $response->getStatusCode() : 200;
    $headers = $response ? $response->getHeaders() : [];
    parent::__construct($data, $status_code, $headers, TRUE);
    $this->url = $url;
  }

  public static function create($data = NULL, $status = 200, $headers = [], CdnLibraryUrl $url = NULL, ResponseInterface $response = NULL) {
    return new static($url, $response);
  }

  /**
   * @return array
   */
  public function getJson() {
    if (!isset($this->json)) {
      $this->json = Json::decode($this->getContent()) ?: [];
    }
    return $this->json;
  }

  /**
   * @return \Drupal\cdn_library\Annotation\CdnLibraryUrl
   */
  public function getUrl() {
    return $this->url;
  }

}
