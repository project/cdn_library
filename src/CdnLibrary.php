<?php

namespace Drupal\cdn_library;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\plus\Utility\AttachmentsInterface;
use Drupal\plus\Utility\Element;

class CdnLibrary {

  /**
   * @var \Drupal\plus\Utility\Attachments
   */
  protected $attachments;

  /**
   * @var \Drupal\cdn_library\CdnLibraryIdentifierInterface
   */
  protected $identifier;

  /**
   * CdnLibrary constructor.
   *
   * @param \Drupal\cdn_library\CdnLibraryIdentifierInterface $identifier
   *   The CdnLibraryIdentifier object associated with this library.
   * @param \Drupal\plus\Utility\AttachmentsInterface $attachments
   *   An Attachments object.
   */
  public function __construct(CdnLibraryIdentifierInterface $identifier, AttachmentsInterface $attachments = NULL) {
    $this->attachments = $attachments;
    $this->identifier = $identifier;
  }

  public static function create(CdnLibraryIdentifierInterface $identifier, AttachmentsInterface $attachments = NULL) {
    return new static($identifier, $attachments);
  }

  public function attachToElement(array &$element = [], callable $asset_callback = NULL) {
    // Set the library that should be attached.
    $element['#attached']['cdnLibrary'][$this->identifier->getProvider()->getPluginId()][] = [$this->identifier->normalizedName(), $this->identifier->normalizedVersion(), $asset_callback];
  }

  public function getAttachments() {
    return $this->attachments;
  }

  public function getCss() {
    return $this->getAttachments()->getAttachedCss();
  }

  public function getJs() {
    return $this->getAttachments()->getAttachedJs();
  }

  /**
   * @param array $element
   *
   * @return array
   *
   * @see callback_cdn_library_attached_assets()
   */
  public static function preRenderCdnLibrary(array $element) {
    $manager = CdnLibraryPluginManager::create();
    $cndLibraries = Element::reference($element)->getAttachment('cdnLibrary');
    $plugin_ids = $cndLibraries->keys();
    $providers = $plugin_ids ? $manager->createInstances($plugin_ids) : [];
    foreach ($providers as $plugin_id => $provider) {
      foreach ($cndLibraries[$plugin_id] as $info) {
        list($name, $version, $asset_callback) = $info;
        $library = $provider->get($name, $version);
        if (is_callable($asset_callback)) {
          $attachments = call_user_func_array($asset_callback, [$library]);
        }
        else {
          $attachments = $library->getAttachments();
        }
        if ($attachments && $attachments instanceof AttachmentsInterface) {
          BubbleableMetadata::createFromRenderArray($element)->addAttachments($attachments->value())->applyTo($element);
        }
      }
    }
    return $element;
  }

}
