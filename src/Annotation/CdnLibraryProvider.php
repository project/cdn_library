<?php

namespace Drupal\cdn_library\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Site\Settings;

/**
 * @Annotation
 */
class CdnLibraryProvider extends Plugin {

  /**
   * The plugin human-readable label.
   *
   * @var string
   */
  protected $label;

  /**
   * The API version.
   *
   * @var int
   */
  protected $version = 0;

  /**
   * A list of base URLs that will be used when necessary.
   *
   * @var \Drupal\cdn_library\Annotation\CdnLibraryUrl[]
   */
  protected $urls = [];

  /**
   * The amount of time, in seconds, that a cached API request should live.
   *
   * Defaults to 604800, one week. Set to 0 to disable caching entirely.
   *
   * @var int
   */
  protected $ttl = 604800;

  /**
   * {@inheritdoc}
   */
  public function __construct($values) {
    // Set a default value for cached TTL based on overridable default setting.
    if (!isset($values['ttl'])) {
      $values['ttl'] = Settings::get('cdn_library_api_request_ttl', 604800);
    }
    parent::__construct($values);
  }

}
