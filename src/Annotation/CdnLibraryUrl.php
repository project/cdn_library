<?php

namespace Drupal\cdn_library\Annotation;

use Drupal\cdn_library\AnnotatedEnum;
use Drupal\cdn_library\CdnLibraryIdentifierInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use GuzzleHttp\Psr7\Uri;

/**
 * @Annotation
 *
 * @Attributes({
 *    @Attribute("value", required = true,  type = "string"),
 *    @Attribute("url",   required = true,  type = "string")
 * })
 */
class CdnLibraryUrl extends AnnotatedEnum {

  use DependencySerializationTrait {
    __sleep as traitSleep;
  }

  const ASSET = 'asset';

  const FILES = 'files';

  const INFO = 'info';

  const VERSIONS = 'versions';

  const SEARCH = 'search';

  /**
   * @var \Drupal\cdn_library\CdnLibraryIdentifierInterface
   */
  protected $identifier;

  /**
   * An associative array of key/value pairs used to replace tokens in the URL.
   *
   * @var array
   */
  protected $rawTokens = [];

  /**
   * The raw URL value.
   *
   * @var string
   */
  public $rawUrl = '';

  /**
   * An associative array of key/value pairs used to replace tokens in the URL.
   *
   * @var array
   */
  protected $tokens = [];

  /**
   * The processed URL.
   *
   * @var string
   */
  protected $url;

  /**
   * An indexed array of found tokens in the raw URL.
   *
   * @var array
   */
  protected $urlTokens = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values = []) {
    parent::__construct($values);
    $this->setUrl($values['url']);
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $this->reset();
    return $this->traitSleep();
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return (string) $this->getUri();
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    return $this;
  }

  public function getIdentifier() {
    return $this->identifier;
  }

  public function getToken($name, $raw = FALSE) {
    $tokens = $this->getTokens($raw);
    return isset($tokens[$name]) ? $tokens[$name] : NULL;
  }

  public function getTokens($raw = FALSE) {
    if ($raw) {
      return $this->rawTokens;
    }

    if (!isset($this->tokens)) {
      $this->tokens = array_map(function ($value) {
        return UrlHelper::encodePath($value);
      }, $this->rawTokens);
    }

    return $this->tokens;
  }

  public function getUrl($raw = FALSE) {
    if ($raw) {
      return $this->rawUrl;
    }

    if (!isset($this->url)) {
      $url = $this->rawUrl ?: '';

      // Find all tokens in the URL.
      $tokens = [];
      preg_match_all('/\{[A-Za-z0-9_-]+}/', $this->rawUrl, $url_tokens);
      $url_tokens = $url_tokens ? reset($url_tokens) : [];
      foreach ($url_tokens as $token) {
        $tokens[substr($token, 1, -1)] = $token;
      }

      if ($url && $tokens) {
        foreach ($tokens as $name => $token) {
          if ($value = $this->getToken($name)) {
            $url = str_replace($token, $value, $url);
          }
          else {
            throw new \InvalidArgumentException(sprintf('The CDN Library Provider URL specifies the "%s" token, but a replacement value was not provided.', $token));
          }
        }
      }

      $this->url = $url;
    }

    return $this->url;
  }

  /**
   * @return \GuzzleHttp\Psr7\Uri
   */
  public function getUri() {
    if (!isset($this->uri)) {
      $this->uri = new Uri($this->getUrl());
    }
    return $this->uri;
  }

  /**
   * @return bool
   */
  public function isValid() {
    try {
      $this->getUri();
      return TRUE;
    }
    catch (\InvalidArgumentException $e) {
      return FALSE;
    }
  }

  protected function reset($identifier = FALSE) {
    if ($identifier) {
      unset($this->identifier);
    }
    unset($this->tokens);
    unset($this->url);
    unset($this->uri);
    return $this;
  }

  public function setIdentifier(CdnLibraryIdentifierInterface $identifier = NULL) {
    if ($identifier) {
      $this->identifier = $identifier;
      $this->setTokens([
        'name' => $identifier->normalizedName($this),
        'version' => $identifier->normalizedVersion($this),
      ]);
    }
    elseif (isset($this->identifier)) {
      $this->reset(TRUE);
    }
    return $this;
  }

  public function setToken($name, $value) {
    $tokens = $this->getTokens();
    $tokens[$name] = $value;
    return $this->setTokens($tokens);
  }

  public function setTokens(array $rawTokens = []) {
    $this->rawTokens = $rawTokens;
    return $this->reset();
  }

  public function setUrl($rawUrl = '') {
    $this->rawUrl = $rawUrl ?: '';
    return $this->reset();
  }

}
