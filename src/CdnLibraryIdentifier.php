<?php

namespace Drupal\cdn_library;

use Composer\Semver\Semver;
use Drupal\cdn_library\Annotation\CdnLibraryUrl;
use Drupal\cdn_library\Plugin\CdnLibraryProviderInterface;

class CdnLibraryIdentifier implements CdnLibraryIdentifierInterface {

  /**
   * The raw name.
   *
   * @var string
   */
  protected $name;

  /**
   * @var \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface
   */
  protected $provider;

  /**
   * @var string
   */
  protected $separator;

  /**
   * The raw version.
   *
   * @var string
   */
  protected $version;

  /**
   * @var array
   */
  protected $versions;

  /**
   * CdnLibraryIdentifier constructor.
   *
   * @param \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface $provider
   * @param $name
   * @param null $version
   * @param array $versions
   */
  public function __construct(CdnLibraryProviderInterface $provider, $name, $version = NULL, array $versions = []) {
    $this->separator = $provider->getIdentifierSeparator();
    if ($version === NULL && strpos($name, $this->separator) > 0) {
      $parts = explode($this->separator, $name);
      $version = array_pop($parts);
      $name = implode($this->separator, $parts);
    }
    try {
      $versions = Semver::satisfiedBy(!$versions && $version ? [$version] : $versions, $version);
    }
    catch (\Exception $e) {
      $versions = [$version];
    }
    $version = array_shift($versions);

    $this->name = $name;
    $this->provider = $provider;
    $this->version = $version;
    $this->versions = $versions;
  }

  /**
   * @return string
   */
  public function __toString() {
    return $this->normalizedName() . $this->separator . $this->normalizedVersion();
  }

  /**
   * @param \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface $provider
   * @param $name
   * @param null $version
   * @param array $versions
   *
   * @return static
   */
  public static function create(CdnLibraryProviderInterface $provider, $name, $version = NULL, array $versions = []) {
    return new static($provider, $name, $version, $versions);
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizedName(CdnLibraryUrl $url = NULL) {
    return $this->provider->normalizeLibraryName($this->name, $url);
  }

  /**
   * {@inheritdoc}
   */
  public function normalizedVersion(CdnLibraryUrl $url = NULL) {
    return $this->provider->normalizeLibraryVersion($this->version, $url);
  }

}
