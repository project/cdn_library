<?php

namespace Drupal\cdn_library\FileCache;

use Drupal\Component\FileCache\FileCacheBackendInterface;

/**
 * Allows to cache data based on file modification dates in PhpStorage.
 */
class FileStorageFileCacheBackend implements FileCacheBackendInterface {

  /**
   * Internal static cache.
   *
   * @var array
   */
  protected static $cache = [];

  /**
   * Bin used for storing the data in the file system.
   *
   * @var string
   */
  protected $bin;

  /**
   * The backend PHP cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a PHP Storage FileCache backend.
   *
   * @param array $configuration
   *   (optional) Configuration used to configure this object.
   */
  public function __construct($configuration) {
    $this->bin = isset($configuration['bin']) ? $configuration['bin'] : 'file_cache';
    $this->cacheBackend = \Drupal::service('cache.backend.chainedfast')->get($this->bin);
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(array $cids) {
    return $this->cacheBackend->getMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function store($cid, $data, $expire = CACHE_PERMANENT) {
    $this->cacheBackend->set($cid, $data, $expire);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($cid) {
    $this->cacheBackend->delete($cid);
  }

}
