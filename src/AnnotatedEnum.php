<?php

namespace Drupal\cdn_library;

use Drupal\Component\Annotation\PluginID;

/**
 * An annotated enumeration class.
 *
 * Originally based from https://github.com/myclabs/php-enum, but modified for
 * use so they can also be used in annotations.
 */
abstract class AnnotatedEnum extends PluginID {

  /**
   * The enumerated constant this instance represents.
   *
   * @var string
   */
  protected $enum;

  /**
   * Store existing constants in a static cache per object.
   *
   * @var array
   */
  protected static $cache = [];

  /**
   * Creates a new value of some type.
   *
   * @param array $values
   *   An array of key/value pairs from the annotation definition.
   *
   * @throws \UnexpectedValueException
   *   When an incompatible type is given.
   */
  public function __construct(array $values = []) {
    $this->value = $values['value'];
    $enum = $this->findEnum($this->value);
    if (!$enum) {
      throw new \UnexpectedValueException(sprintf('The provided value "%s" is not part of the enumerated class: %s', $this->value, get_called_class()));
    }
    $this->enum = $enum;
  }

  /**
   * Creates a new value of some type.
   *
   * @param mixed $values
   *   The values that belongs to a constant on the enumerated class.
   *
   * @return static
   *
   * @throws \UnexpectedValueException
   *   When an incompatible type is given.
   */
  public static function create($values) {
    return new static($values);
  }

  /**
   * Returns a value when called statically like so: MyEnum::SOME_VALUE() given
   * SOME_VALUE is a class constant
   *
   * @param string $name
   * @param array $arguments
   *
   * @return static
   * @throws \BadMethodCallException
   */
  public static function __callStatic($name, $arguments) {
    $enum = static::getConstants();
    if (isset($enum[$name])) {
      return new static($enum[$name]);
    }

    throw new \BadMethodCallException("No static method or enum constant '$name' in class " . get_called_class());
  }

  /**
   * @return string
   */
  public function __toString() {
    return (string) $this->enum;
  }

  /**
   * The name of the constant that represents this instance.
   *
   * @return string
   */
  public function getEnum() {
    return $this->enum;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->value;
  }

  /**
   * Returns all possible values as an array
   *
   * @return array Constant name in key, constant value in value
   */
  public static function getConstants() {
    $class = get_called_class();
    if (!array_key_exists($class, static::$cache)) {
      $reflection = new \ReflectionClass($class);
      static::$cache[$class] = $reflection->getConstants();
    }

    return static::$cache[$class];
  }

  /**
   * Return key for value
   *
   * @param mixed $value
   *   The value of an enumerated constant.
   *
   * @return string|false
   *   The name of the enumerated constant or FALSE if constant doesn't exist.
   */
  public static function findEnum($value) {
    return array_search($value, static::getConstants(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function setClass($class) {
    $this->class = $class;
  }

  /**
   * {@inheritdoc}
   */
  public function setProvider($provider) {
    $this->provider = $provider;
  }

}
