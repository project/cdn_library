<?php

namespace Drupal\cdn_library;

use Drupal\Core\File\FileSystemInterface;

interface CdnLibraryFileSystemInterface extends FileSystemInterface {

}
