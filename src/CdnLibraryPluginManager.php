<?php

namespace Drupal\cdn_library;

use Drupal\cdn_library\Annotation\CdnLibraryProvider;
use Drupal\cdn_library\Plugin\CdnLibraryProviderInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\Extension;
use Drupal\plus\Plugin\PluginProviderTypeInterface;
use Drupal\plus\ProviderPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CdnLibraryPluginManager.
 */
class CdnLibraryPluginManager extends ProviderPluginManager {

  /**
   * An extension responsible for making any external HTTP requests.
   *
   * @var \Drupal\Core\Extension\Extension
   */
  protected $requester;

  /**
   * {@inheritdoc}
   */
  public function __construct(PluginProviderTypeInterface $provider_type, CacheBackendInterface $cache_backend, Extension $requester = NULL) {
    parent::__construct($provider_type, 'Plugin/CdnLibrary', CdnLibraryProviderInterface::class, CdnLibraryProvider::class, $cache_backend);
    $this->alterInfo('cdn_library_providers');
    $this->requester = $requester;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container = NULL, Extension $requester = NULL) {
    if (!isset($container)) {
      $container = \Drupal::getContainer();
    }
    return new static(
      $container->get('plugin.providers'),
      $container->get('cache.discovery'),
      $requester
    );
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface
   */
  public function createInstance($plugin_id, array $configuration = []) {
    if (!isset($configuration['requester'])) {
      $configuration['requester'] = $this->requester;
    }
    /** @var \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface $instance */
    $instance = parent::createInstance($plugin_id, $configuration);
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface[]
   */
  public function createInstances(array $plugin_ids = [], array $configuration = []) {
    if (!isset($configuration['requester'])) {
      $configuration['requester'] = $this->requester;
    }
    /** @var \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface[] $instances */
    $instances = parent::createInstances($plugin_ids, $configuration);
    return $instances;
  }

  /**
   * Retrieves the first available library from all available providers.
   *
   * @param string $name
   *   The library name to retrieve.
   * @param string $version
   *   Optional. A specific version or version like constraints to return.
   *
   * @return \Drupal\cdn_library\CdnLibrary|null
   *   A CdnLibrary object or NULL if none could be found.
   */
  public function get($name, $version = NULL) {
    $found = NULL;
    /** @var \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface[] $providers */
    $providers = $this->createInstances();
    foreach ($providers as $provider) {
      if ($library = $provider->get($name, $version)) {
        $found = $library;
        break;
      }
    }
    return $found;
  }

  /**
   * Sets the extension responsible for making any external HTTP requests.
   *
   * @param string|\Drupal\Core\Extension\Extension $type
   *   The extension type, e.g. module, theme, etc.
   * @param string $name
   *   The machine name of the extension. Optional if an Extension object was
   *   passed as $type.
   *
   * @return static
   *
   * @throws \InvalidArgumentException
   *   When provided argument(s) do not resolve to a valid extension.
   */
  public function setRequester($type, $name = NULL) {
    // Handle Extension objects.
    if ($type instanceof Extension) {
      $name = $type->getName();
      $type = $type->getType();
    }

    // @todo Replace with proper service in 8.6.x.
    // @see https://www.drupal.org/node/2709919
    if (!is_string($type) || !is_string($name) || !($list = system_list($type)) || !isset($list[$name])) {
      throw new \InvalidArgumentException(sprintf('Passed arguments must resolve to a valid extension. Unknown "%s" extension "%s".', (string) $type, (string) $name));
    }

    $this->requester = $list[$name];

    return $this;
  }

}
