<?php

namespace Drupal\cdn_library;

use Drupal\Component\FileCache\FileCacheFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Psr\Log\LoggerInterface;

class CdnLibraryFileSystem extends FileSystem implements CdnLibraryFileSystemInterface {

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * @var \Drupal\Component\FileCache\FileCacheInterface
   */
  protected $fileCache;

  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager, Settings $settings, LoggerInterface $logger, CacheBackendInterface $cache_backend) {
    parent::__construct($stream_wrapper_manager, $settings, $logger);
    $this->cache = $cache_backend;
    $this->fileCache = FileCacheFactory::get('cdn_library');
  }

}
