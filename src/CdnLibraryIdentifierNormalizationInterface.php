<?php

namespace Drupal\cdn_library;

use Drupal\cdn_library\Annotation\CdnLibraryUrl;

interface CdnLibraryIdentifierNormalizationInterface {

  /**
   * The separator used between the library name and version.
   *
   * @return string
   */
  public function getIdentifierSeparator();

  /**
   * Normalizes a library name.
   *
   * @param string $name
   *   The library name to normalize.
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl $url
   *   The URL for which this name will be used in, if any, for context.
   *
   * @return string
   */
  public function normalizeLibraryName($name, CdnLibraryUrl $url = NULL);

  /**
   * Normalizes a library version.
   *
   * @param string $version
   *   The library version to normalize.
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl $url
   *   The URL for which this version will be used in, if any, for context.
   *
   * @return string
   */
  public function normalizeLibraryVersion($version, CdnLibraryUrl $url = NULL);

}
