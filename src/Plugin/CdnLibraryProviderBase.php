<?php

namespace Drupal\cdn_library\Plugin;

use Composer\Semver\Semver;
use Drupal\cdn_library\Annotation\CdnLibraryUrl;
use Drupal\cdn_library\CdnLibraryIdentifier;
use Drupal\cdn_library\CdnLibraryIdentifierInterface;
use Drupal\cdn_library\CdnLibraryResponse;
use Drupal\Component\Utility\Crypt;
use Drupal\plus\Plugin\ThemePluginBase;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

abstract class CdnLibraryProviderBase extends ThemePluginBase implements CdnLibraryProviderInterface {

  /**
   * @var string
   */
  public static $defaultUserAgent;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public static $moduleHandler;

  /**
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  public static $themeHandler;

  /**
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  public static $themeManager;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The plugin specific HTTP client used to make API requests.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The extension that is going to be making requests.
   *
   * @var \stdClass
   */
  protected $requester;


  protected $providerUserAgent;

  /** @noinspection PhpMissingParentConstructorInspection */

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);

    $this->cache = \Drupal::service('cache.cdn_library');
    if (isset($configuration['requester'])) {
      $this->requester = $configuration['requester'];
    }
  }

  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $this->moduleHandler()->alter($type, $data, $context1, $context2);

    // @todo Re-enable theme alters once Backport has fixed ThemeManager.
    if ((int) \Drupal::VERSION[0] >= 8) {
      $this->themeManager()->alter($type, $data, $context1, $context2);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];

    // Add this plugin's provider as a dependency.
    $provider = $this->pluginDefinition['provider'];

    // @todo Refactor this once provider type based plugins are a thing.
    $type = !$this->moduleHandler()->getModule($provider) && ($theme = $this->themeHandler()->getTheme($provider)) ? 'theme' : 'module';
    $dependencies[$type][] = $provider;

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['requester' => NULL];
  }

  /**
   * Initiates an HTTP request.
   *
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl $url
   *   The URL to request.
   * @param array $options
   *   Any additional options to pass to the HTTP client.
   * @param string $method
   *   Optional. The HTTP method to use, defaults to GET.
   *
   * @return \Drupal\cdn_library\CdnLibraryResponse
   *   A CdnLibraryResponse object.
   */
  protected function doRequest(CdnLibraryUrl $url, array $options = [], $method = 'GET') {
    try {
      $response = $this->httpClient()->request($method, $url->getUri(), $options);
    }
    catch (RequestException $e) {
      $response = new Response(400);
    }
    return new CdnLibraryResponse($url, $response);
  }

  protected function httpClient() {
    if (!isset($this->httpClient)) {
      $this->httpClient = \Drupal::service('http_client_factory')->fromOptions(['headers' => ['User-Agent' => $this->userAgent()]]);
    }
    return $this->httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name, $version = NULL) {
    $versions = $this->getVersions($name, $version);
    $identifier = CdnLibraryIdentifier::create($this, $name, $version, $versions);
    $url = $this->getUrl('files', $identifier);

    // Immediately return if the CdnLibraryProvider cannot be searched.
    if (!$url->isValid()) {
      return [];
    }

    $response = $this->request($url);
    return $this->parseResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * Retrieves a URL that will be used for a specific action.
   *
   * @param string $type
   *   The type of URL defined by the plugin.
   * @param \Drupal\cdn_library\CdnLibraryIdentifierInterface $identifier
   *   A CdnLibraryIdentifier, if any.
   *
   * @return \Drupal\cdn_library\Annotation\CdnLibraryUrl
   *   The normalized URL or an empty string if the action provided is invalid.
   */
  protected function getUrl($type, CdnLibraryIdentifierInterface $identifier = NULL) {
    $found = NULL;
    $urls = $this->pluginDefinition['urls'] ?: [];
    /** @var \Drupal\cdn_library\Annotation\CdnLibraryUrl $url */
    foreach ($urls as $url) {
      if ($url->getType() === $type) {
        $found = clone $url;
        break;
      }
    }
    if ($found && $identifier) {
      $found->setIdentifier($identifier);
    }
    return $found;
  }

  public function getVersions($name, $constraints = NULL) {
    $identifier = CdnLibraryIdentifier::create($this, $name);
    $version = $identifier->normalizedVersion();

    $url = $this->getUrl('versions', $identifier);
    $response = $this->request($url);
    $versions = $this->parseResponse($response);
    if ($version || $constraints) {
      $versions = Semver::satisfiedBy($versions, $version ?: $constraints);
    }
    $sorted = Semver::rsort($versions);
    return $sorted;
  }

  /**
   * Retrieves the Module Handler service.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The Module Handler service.
   */
  protected function moduleHandler() {
    if (!isset(static::$moduleHandler)) {
      static::$moduleHandler = \Drupal::service('module_handler');
    }
    return static::$moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeLibraryName($name, CdnLibraryUrl $url = NULL) {
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeLibraryVersion($version, CdnLibraryUrl $url = NULL) {
    return $version;
  }

  /**
   * Parses the JSON response of a search query.
   *
   * @param \Drupal\cdn_library\CdnLibraryResponse $response
   *   The CdnLibraryResponse object.
   *
   * @return mixed
   *   An array of CdnLibraryResult objects.
   */
  abstract protected function parseResponse(CdnLibraryResponse $response);

  /**
   * Retrieves a cached HTTP request, initiating a new one as needed.
   *
   * @param \Drupal\cdn_library\Annotation\CdnLibraryUrl $url
   *   The URL to request.
   * @param array $options
   *   Any additional options to pass to the HTTP client.
   * @param string $method
   *   Optional. The HTTP method to use, defaults to GET.
   *
   * @return \Drupal\cdn_library\CdnLibraryResponse
   *   A CdnLibraryResponse object.
   */
  protected function request(CdnLibraryUrl $url, array $options = [], $method = 'GET') {
    $cid = "$method:$url:" . Crypt::hashBase64(serialize($options));
    $expires = (int) $this->pluginDefinition['ttl'];
    if ($expires) {
      $expires += \Drupal::time()->getRequestTime();
    }

    // Use a cached response, if one exists.
    if ($expires && ($cache = $this->cache->get($cid)) && isset($cache->data)) {
      $response = $cache->data;
    }
    else {
      $response = $this->doRequest($url, $options, $method);

      // Cache the response.
      if ($expires) {
        $this->cache->set($cid, $response, $expires, ['cdn_library:' . $this->pluginId]);
      }
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function search($query = '') {
    $url = $this->getUrl('search')->setToken('query', $query);

    // Immediately return if the CdnLibraryProvider cannot be searched.
    if (!$url->isValid()) {
      return [];
    }

    $response = $this->request($url);
    return $this->parseResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  protected function userAgent() {
    // Determine the default base user agent.
    if (!isset(static::$defaultUserAgent)) {
      // @todo Replace with proper service in 8.6.x.
      // @see https://www.drupal.org/node/2709919
      $info = system_get_info('module', 'cdn_library');
      static::$defaultUserAgent = $info['name'] . '/' . (isset($info['version']) ? $info['version'] : \Drupal::CORE_COMPATIBILITY . '-dev') . ' (+https://www.drupal.org/project/cdn_library/) ';
      static::$defaultUserAgent .= \Drupal::service('http_client')->getConfig('headers')['User-Agent'];
    }

    // Prepend the CDN Library Provider specific UserAgent data.
    if (!isset($this->providerUserAgent)) {
      // Use the default user agent.
      $this->providerUserAgent = static::$defaultUserAgent;

      // @todo Refactor once provider specific plugins can be made.
      // @todo Replace with proper service in 8.6.x.
      // @see https://www.drupal.org/node/2709919
      $provider = $this->pluginDefinition['provider'];
      $provider_info = system_get_info('module', $provider);
      if (!$provider_info) {
        $provider_info = system_get_info('theme', $provider);
      }
      if ($provider_info) {
        $this->providerUserAgent = $provider_info['name'] . '/' . (!empty($provider_info['version']) ? $provider_info['version'] : \Drupal::CORE_COMPATIBILITY . '-dev') . " (+https://www.drupal.org/project/$provider/) " . $this->providerUserAgent;
      }

      // Add a specific requester to the UserAgent data.
      if (isset($this->requester)) {
        $this->providerUserAgent = $this->requester->info['name'] . '/' . (!empty($this->requester->info['version']) ? $this->requester->info['version'] : \Drupal::CORE_COMPATIBILITY . '-dev') . " (+https://www.drupal.org/project/{$this->requester->name}/) " . $this->providerUserAgent;
      }
    }

    return $this->providerUserAgent;
  }


  /**
   * Retrieves the Theme Handler service.
   *
   * @return \Drupal\Core\Extension\ThemeHandlerInterface
   *   The Theme Handler service.
   */
  protected function themeHandler() {
    if (!isset(static::$themeHandler)) {
      static::$themeHandler = \Drupal::service('theme_handler');
    }
    return static::$themeHandler;
  }

  /**
   * Retrieves the Theme Manager service.
   *
   * @return \Drupal\Core\Theme\ThemeManagerInterface
   *   The Theme Manager service.
   */
  protected function themeManager() {
    if (!isset(static::$themeManager)) {
      static::$themeManager = \Drupal::service('theme.manager');
    }
    return static::$themeManager;
  }

}
