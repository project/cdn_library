<?php

namespace Drupal\cdn_library\Plugin;

use Drupal\cdn_library\CdnLibraryIdentifierNormalizationInterface;
use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

interface CdnLibraryProviderInterface extends PluginInspectionInterface, ConfigurablePluginInterface, ContainerFactoryPluginInterface, CdnLibraryIdentifierNormalizationInterface {

  /**
   * Retrieves a specific library.
   *
   * @param string $name
   *   The name of the library to retrieve. The library name may also end with
   *   a specific version separated by an at symbol (@).
   * @param string $version
   *   A specific version to retrieve. If omitted, the latest stable
   *   version that can be determined will be used. This includes any version
   *   specified in $name.
   *
   * @return \Drupal\cdn_library\CdnLibrary|null
   *   A CdnLibrary object or NULL if there is no such library.
   */
  public function get($name, $version = NULL);

  /**
   * Searches for a specific library.
   *
   * @param string $name
   *   The name of the library to find.
   *
   * @return \Drupal\cdn_library\CdnLibraryResult[]
   *   An array of CdnLibraryResult objects that matched the search criteria.
   */
  public function search($name);

}
