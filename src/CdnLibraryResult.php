<?php

namespace Drupal\cdn_library;

use Drupal\cdn_library\Plugin\CdnLibraryProviderInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

class CdnLibraryResult {

  use DependencySerializationTrait;

  /**
   * The name of the library asset.
   *
   * @var string
   */
  protected $name;

  /**
   * The raw JSON data of the search result.
   *
   * Note: its structure is arbitrarily based on the search provider.
   *
   * @var array
   */
  protected $json;

  /**
   * The CdnLibraryProvider plugin instance for which this result belongs to.
   *
   * @var \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface
   */
  protected $provider;

  /**
   * CdnLibraryResult constructor.
   *
   * @param string $name
   *   The name of the library asset.
   * @param \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface $provider
   *   The CdnLibraryProvider plugin instance for which this result belongs to.
   * @param array $json
   *   The raw JSON data of the search result.
   */
  public function __construct($name, CdnLibraryProviderInterface $provider, array $json = []) {
    $this->name = $name;
    $this->provider = $provider;
    $this->json = $json;
  }

  /**
   * Creates a new CdnLibraryResult object.
   *
   * @param string $name
   *   The name of the library asset.
   * @param \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface $provider
   *   The CdnLibraryProvider plugin instance for which this result belongs to.
   * @param array $json
   *   The raw JSON data of the search result.
   *
   * @return static
   */
  public static function create($name, CdnLibraryProviderInterface $provider, array $json = []) {
    return new static($name, $provider, $json);
  }

  /**
   * Retrieves the name of the library asset.
   *
   * @return string
   *   The name of the library asset.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Retrieves the raw JSON data of the search result.
   *
   * @return array
   *   The raw JSON data of the search result.
   */
  public function getJson() {
    return $this->json;
  }

  /**
   * Retrieves the CDN Library Provider plugin instance.
   *
   * @return \Drupal\cdn_library\Plugin\CdnLibraryProviderInterface
   *   The CdnLibraryProvider plugin instance for which this result belongs to.
   */
  public function getProvider() {
    return $this->provider;
  }

}
