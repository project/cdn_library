<?php

/**
 * @file
 * CDN Library API.
 */

use Drupal\cdn_library\CdnLibrary;
use Drupal\plus\Utility\Attachments;

/**
 * Provides assets from a CdnLibrary that will be attached to an element.
 *
 * Callback for \Drupal\cdn_library\CdnLibrary::preRenderCdnLibrary().
 *
 * @param \Drupal\cdn_library\CdnLibrary $library
 *   The CdnLibrary containing assets.
 *
 * @return array
 *   An attachment array containing necessary assets from the library.
 *
 * @ingroup callbacks
 */
function callback_cdn_library_attached_assets(CdnLibrary $library) {
  $attachments = Attachments::create();
  if ($css = $library->getCss()) {
    // Do custom CSS filtering here.
    $attachments->attach('css', $css);
  }
  if ($js = $library->getJs()) {
    // Do custom JS filtering here.
    $attachments->attach('js', $js);
  }
  return $attachments;
}
